#!/bin/bash
## Jonathan Compton, 2019

# Strict error checking
set -o nounset
set -o pipefail
set -o errtrace

# For debugging
#set -o xtrace

readonly SERVER_LOG=log/dc.log

get_logs() {
  docker-compose ps | tail -n +3 | awk '{print $1}' | xargs -n1 docker logs
}

setup() {
  echo bringing the containers down
  docker-compose down
  echo building the container and bringing it up if it succeeds
  #docker-compose build
  docker-compose up --detach

  logs="$(get_logs)"
  while [[ ! "$logs" =~ INFO\ \ WEBrick::HTTPServer#start: ]]; do
    echo waiting for app service to come up
    sleep 1
    logs="$(get_logs)"
  done

  echo 'container finished coming up'
  echo attempting to rake db:reset the test db...
  # Need to drop db so test db gets dropped correctly
  docker-compose run web rake db:drop
  docker-compose run -e "RAILS_ENV=test" web rake db:reset
  echo 'returning from setup()'
}

run_specs() {
  local results="$(docker-compose run -e "RAILS_ENV=test" web rspec spec/models/app_instance_spec.rb 2>&1)"
  local ret_code=-1

  local bad_target="undefined.*method.*exec_cmd.*for.*nil:NilClass"
  local good_target="0 failures"
  if [[ "$results" =~ $bad_target ]]; then
    ret_code=1
  elif [[ "$results" =~ $good_target ]]; then
    ret_code=0
  else
    ret_code=125
  fi

  echo $ret_code
}

main() {
  setup
  ret="$(run_specs)"
# These echos break the return. TODO: fix so we get verbosity, but good returns.
#  if [[ "$ret" -eq 1 ]]; then
#    echo "----- bad commit: $(git log --pretty=format:'%h' -n 1) -------"
#  elif [[ "$ret" -eq 0 ]]; then
#    echo "----- good commit: $(git log --pretty=format:'%h' -n 1) -------"
#  elif [[ "$ret" -eq 125 ]]; then
#    echo "----- couldn't test. skipping: $(git log --pretty=format:'%h' -n 1) -------"
#  else
#    echo "something unexpected happened. boo."
#  fi

  
}
main

exit $ret
